from django.contrib import admin
from comments.constants import Constants
from .models import Flag, Comment
from django.contrib import messages
from django.utils.translation import ngettext
from django.contrib.admin import SimpleListFilter # TODO: some filtering
from django.contrib.auth.models import User, Group
import logging

admin.site.unregister(Group)
admin.site.unregister(User)

# Create the logger.
logger = logging.getLogger(__name__)

def get_model_perms(self, request):
    ## check if the request.user should see model, if not:
    return {'change': False, 'add': False}

class FlagAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,			{'fields': ['reason']}),
        ('State',		{'fields': ['state']}),
        ('Comment',		{'fields': ['comment']}),
        ('Flagger',		{'fields': ['flagger']}),
    ]

    list_display = ('state','flagger', 'commenter', 'date_created', 'reason', 'comment_text')

    search_fields = ['comment__content']

    readonly_fields = ['comment', 'flagger', 'reason']

    list_per_page = 20

    def mark_as_spam(self, request, queryset):
    	updated = queryset.update(state=Constants.STATE_SPAM)
    	for f in queryset:
    		f.comment.state=Constants.STATE_DELETED
    		f.comment.save()

    	self.message_user(request, ngettext('%d flag was successfully marked as spam.','%d flags were successfully marked as spam.',updated,
    		) % updated, messages.SUCCESS)

    def mark_as_invalid(self, request, queryset):
    	updated = queryset.update(state=Constants.STATE_INVALID)
    		
    	self.message_user(request, ngettext('%d flag was successfully marked as invalid.','%d flags were successfully marked as invalid.',updated,
    		) % updated, messages.SUCCESS)

    def rules_violation(self, request, queryset):
    	updated = queryset.update(state=Constants.STATE_RULES)
    	for f in queryset:
    		f.comment.state=Constants.STATE_RULES
    		f.comment.save()

    	self.message_user(request, ngettext('%d flag was successfully marked as a rules violation.','%d flags were successfully marked as violating rules.',updated,
    		) % updated, messages.SUCCESS)

    def illegal(self, request, queryset):
    	updated = queryset.update(state=Constants.STATE_ILLEGAL)
    	for f in queryset:
    		f.comment.state=Constants.STATE_ILLEGAL
    		f.comment.save()

    	self.message_user(request, ngettext('%d flag was successfully marked as illegal.','%d flags were successfully marked as illegal.',updated,
    		) % updated, messages.SUCCESS)

    actions = ['mark_as_spam', 'mark_as_invalid', 'rules_violation', 'illegal']


#admin.site.unregister(Group)
#admin.site.unregister(Site)
admin.site.register(Flag, FlagAdmin)


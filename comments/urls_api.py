from django.urls import path
from . import views_api

urlpatterns = [
    path('get_comments/', views_api.get_comments, name='get_comments'),
    path('create_comment/', views_api.create_comment, name='create_comment'),
    path('update_comment/', views_api.update_comment, name='update_comment'),
    path('upvote_comment/', views_api.upvote_comment, name='upvote_comment'),
    path('delete_comment/', views_api.delete_comment, name='delete_comment'),
    path('flag_comment/', views_api.flag_comment, name='flag_comment'),
    path('get_comment_count/', views_api.get_comment_count, name='get_comment_count'),
]
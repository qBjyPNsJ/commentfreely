from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.conf import settings
import base64, binascii, time, hmac, hashlib, json
import logging
from Crypto.PublicKey import RSA

# Create the logger.
logger = logging.getLogger(__name__)

def index(request):
	logger.info("demo")

	#client_ip = request.META['REMOTE_ADDR']
	#logger.warn(client_ip)

	#profile_id = '223456789014'
	profile_id = '323456789012'
	owner_id = '123456789012'
	display_name = 'justsomeguy'
	thread_id = '223456789012'
	icon_url = 'https://robohash.org/set_set4/bgset_bg1/'+profile_id+'?size=50x50'
	is_thread_admin = True

	cf_raw = {
	    'profile_id': profile_id,
	    'owner_id': owner_id,
	    'display_name': display_name,
	    'thread_id': thread_id,
	    'icon_url': icon_url,
	}
	cf_timestamp = int(time.time())
	cf_json = json.dumps(cf_raw)
	cf_data = str(base64.b64encode(cf_json.encode('ascii')), 'utf-8')
	cf_message = cf_data + ' ' + str(cf_timestamp)
	cf_signature = hmac.HMAC(settings.API_SECRET.encode('utf-8'), cf_message.encode('utf-8'), hashlib.sha256).hexdigest()
	cf_auth = '%s %s %s' % (cf_data, cf_signature, cf_timestamp)
	cf_url = 'http://localhost:9000' # cf_url = 'https://cf-staging.bitchute.com:'
	return render(request, 'comments/index.html', {'thread_id': thread_id, 'cf_auth': cf_auth, 'cf_url': cf_url, 'current_user_id': profile_id, 'icon_url': icon_url, 'is_thread_admin': is_thread_admin})
	#return render(request, 'comments/index.html', {})